package com.mark.service;

public interface Button<T> {
	public	T render();
	public	T createButton();
}
